/**
 * The cookie problem
 *
 * For ex02 (and ex02bis) we are supposed to store user info in cookies, but due
 * to security reasons browsers (Firefox/Chrome) won't allow JavaScript to
 * access cookies after a session ends (user closes tab/browser) if the document
 * running JavaScript is not on a server (i.e openning a .html file directly).
 *
 * There could be two solutions for this problem, either serving the file from
 * a server (like PAMP or Express) or saving the information in a different way.
 *
 * Up to commit b62337079366073614ee800c5c0d5dd1aec719f4 my implementation uses
 * cookies and can be deployed on a server normally. This version however uses
 * localStorage to set and get the data.
 */

/**
 * Get the tasks from local storage
 * @returns {Array}
 */
function getTasks()
{
    let arr = localStorage.getItem('tasks');
    arr     = (arr) ? JSON.parse(arr) : new Array();

    return (arr);
}
/**
 * Store tasks in local storage
 * @param {Array} tasks array containing the tasks
 */
function setTasks(tasks)
{
    localStorage.setItem('tasks', JSON.stringify(tasks));
}

window.onload = () => {

    let newButton = document.getElementById('new-button');
    let list      = document.getElementById('ft_list');

    function removeTask(task) {
        if (window.confirm('Remove task?'))
        {
            let arr = getTasks();
            let i   = arr.indexOf(task.getAttribute('data-content'));

            if (i != -1) {
                arr.splice(i, 1);
                setTasks(arr);
            }

            task.remove();
        }
    }

    function addTask(task, append = false) {
        let node = document.createElement('div');
        node.classList.add('task');

        node.innerText = task;
        node.setAttribute('data-content', task);
        node.addEventListener('click', () => removeTask(node));

        if (append)
            list.append(node);
        else
            list.prepend(node);
    }

    function buildUI() {
        let arr = getTasks();

        arr.forEach((task) => addTask(task));
    }

    newButton.addEventListener('click', (_) => {
        let task = window.prompt('New task');

        if (task && task.trim() != '') {
            let arr = getTasks();

            if (arr.indexOf(task) != -1)
                return window.alert('Task exists.');

            arr.push(task);
            setTasks(arr);

            addTask(task);
        }
    });

    buildUI();
};
