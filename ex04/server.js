const express    = require('express');
const app        = express();
const path       = require('path');
const bodyParser = require('body-parser');
const fs         = require('fs');
const crypto     = require('crypto');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

function getData() {
    try {
        let content = fs.readFileSync('list.json').toString();
        let data    = JSON.parse(content);

        return (data || new Array());
    }
    catch(e) {
        console.error(e);
        return new Array();
    }
}

function setData(data) {
    try {
        fs.writeFileSync('list.json', JSON.stringify(data, null, 2));
        return true;
    }
    catch(e) {
        console.error(e);
        return false;
    }
}

app.use(express.static(path.join(__dirname, 'public')));

app.post('/task/add', async (req, res) => {
    // if (req.body.task)

    if (!req.body.task)
        return res.status(400).json({
            error: 'NO_TASK',
            msg: 'Task must be specified.'
        });

    try {
        let data = getData();
        let task = {
            id: crypto.randomBytes(20).toString('hex'),
            task: req.body.task
        };

        data.push(task);
        setData(data);

        res.status(200).json(task);
    }
    catch(e) {
        console.error(e);
        res.status(500).json({ error: 'INTERNAL_ERROR', data: e });
    }
});

app.get('/task/list', async (req, res) => {
    try {
        let list = getData();
        res.status(200).json(list);
    }
    catch(e) {
        console.error(e);
        res.status(500).json({ error: 'INTERNAL_ERROR', data: e });
    }
});

app.delete('/task/:id', (req, res) => {
    let list = getData();

    list = list.filter(val => val.id != req.params.id);
    setData(list);

    res.status(200).end();
});

app.listen(process.env.PORT || 3000, function () {
    console.log(`Server running on port ::${this.address().port}`);
});
