
async function getTasks()
{
    let res = await $.get('/task/list');
    return (res);
}

async function pushTask(task)
{
    try {
        let res = await $.post('/task/add', { task: task });
        return (res);
    }
    catch(e) {
        window.alert(e);
        return (false);
    }
}

async function deleteTask(taskId)
{
    await $.ajax({ url: '/task/' + taskId, type: 'DELETE' });
}

$(window).on('load', function () {

    let $newButton = $('#new-button');
    let $list      = $('#ft_list');

    async function removeTask(node) {
        if (window.confirm('Remove task?'))
        {
            await deleteTask(node.getAttribute('data-id'));
            node.remove();
        }
    }

    function addTask(task, append = false) {
        let node = document.createElement('div');
        node.classList.add('task');

        node.innerText = task.task;
        node.setAttribute('data-id', task.id);

        node.addEventListener('click', async () => await removeTask(node));

        if (append)
            $list.append(node);
        else
            $list.prepend(node);
    }

    async function buildUI() {
        let arr = await getTasks();

        arr.forEach((task) => addTask(task));
    }

    $newButton.on('click', async function () {
        let task = window.prompt('New task');

        if (task && task.trim() != '') {
            // let arr = getTasks();

            // if (arr.indexOf(task) != -1)
            //     return window.alert('Task exists.');

            // arr.push(task);

            let t = await pushTask(task);
            if (t)
                addTask(t);
        }
    });

    buildUI();
    // getTasks();
});
